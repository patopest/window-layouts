#!/usr/bin/env python3

import argparse
from datetime import datetime
import json
import logging
import os
import subprocess
import sys
import time


logger = logging.getLogger("App")


def run_command(cmd):
	try:
		logger.debug("Running cmd: \"{}\"".format(cmd))
		p = subprocess.run(cmd, shell=True, check=True, capture_output=True)
		return p.stdout
	except subprocess.CalledProcessError as err:
		logger.error("{} -> {}".format(err, err.stderr))
		return None


def get_displays():
	output = run_command("yabai -m query --displays")
	return json.loads(output)

def get_windows():
	output = run_command("yabai -m query --windows")
	return json.loads(output)

def get_spaces():
	output = run_command("yabai -m query --spaces")
	return json.loads(output)

def get_space(window_id):
	output = run_command("yabai -m query --spaces --window {}".format(window_id))
	return json.loads(output)


def check_window(window, target):
	action = False

	if (window["space"] != target["space"]):
		action = True

	w_frame = window["frame"]
	t_frame = target["frame"]

	if (w_frame != t_frame):
		action = True

	# if (window["is-native-fullscreen"] != target["is-native-fullscreen"]):
	# 	action = True

	return action


def check_fullscreen_windows(spaces, windows):

	for s in spaces:
		space = s
		if (space["is-native-fullscreen"]):
			window_id = space["windows"][0]

			window = None
			for w in windows:
				if (w["id"] == window_id):
					window = w
					break

			if (window):
				display = window["display"]
				if (display != space["display"]):
					logger.debug("Moving full screen space {} to display {}".format(space["index"], display))
					cmd = "yabai -m space {} --display {}".format(space["index"], display)
					run_command(cmd)

					# getting new space index after it being moved
					space = get_space(window_id)[0]

				if (space["index"] != window["space"]):
					cmd = "yabai -m space {} --move {}".format(space["index"], window["space"])
					run_command(cmd)


def move_window(window, target):
	window_id = window["id"]
	space = target["space"]
	frame = target["frame"]
	pos = (frame["x"], frame["y"])
	size = (frame["w"], frame["h"])

	if (window["frame"]["w"] > target["frame"]["w"] or window["frame"]["h"] > target["frame"]["h"]):
		scale_down = True
	else:
		scale_down = False

	logger.info("Moving window {} to space {}, pos:{}, size:{}, scale_down:{}".format(window_id, space, pos, size, scale_down))

	# if target size is smaller than current window, resize it first (avoids bug where window gets outside of display)
	if scale_down == True:
		cmd = "yabai -m window {} --resize abs:{}:{}".format(window_id, size[0], size[1])
		run_command(cmd)

	cmd = "yabai -m window {} --space {}".format(window_id, space)
	run_command(cmd)
	cmd = "yabai -m window {} --move abs:{}:{}".format(window_id, pos[0], pos[1])
	run_command(cmd)

	if scale_down == False:
		cmd = "yabai -m window {} --resize abs:{}:{}".format(window_id, size[0], size[1])
		run_command(cmd)




def main():
	parser = argparse.ArgumentParser(description="Utility to save and restore window layouts using yabai")
	a = parser.add_argument_group("App")
	a.add_argument("--save-layout", "--save", "-s", action="store_true", help="Save the current window layout")
	a.add_argument("--restore-layout", "--restore", "-r", action="store_true", help="Restore a window layout")
	a.add_argument("--file", "-f", type=str, default=None, help="File to save or load layout (default is based on number of displays)")

	c = parser.add_argument_group("Config")
	c.add_argument("--config-dir", type=str, default=".yabai", help="Directory where layout files are located (default %(default)s)")

	l = parser.add_argument_group("Log")
	l.add_argument("--log-level", type=str, default="DEBUG", help="Log level (default %(default)s)")
	l.add_argument("--log-format", type=str, default="%(asctime)s - %(levelname)s - %(message)s", help="Log format (default \"%(default)s\")")
	l.add_argument("--log-file", type=str, default="log.txt", help="File to store logs (default %(default)s)")
	l.add_argument("--verbose", "-v", action="store_true", help="Log to stdout too")
	args = parser.parse_args()


	log_file = os.path.join(os.environ['HOME'], args.config_dir, args.log_file)
	log_handlers = [logging.FileHandler(log_file)]
	if (args.verbose):
		log_handlers.append(logging.StreamHandler(sys.stdout))

	logging.basicConfig(format=args.log_format, level=args.log_level, handlers=log_handlers)

	logger.info("")

	displays = get_displays()
	windows = get_windows()
	spaces = get_spaces()

	current_layout = {
		"time": datetime.now().strftime("%m-%d-%Y %H:%M:%S"),
		"displays": displays,
		"windows": windows
	}

	config_dir = os.path.join(os.environ['HOME'], args.config_dir)
	if (not os.path.exists(config_dir)):
		os.mkdir(config_dir)

	if (args.file):
		file_name = args.file
	else:
		file_name = "yabai_layout_{}_displays.json".format(len(displays))

	# Creating file name with full path
	file_name = os.path.join(config_dir, file_name)


	if (args.save_layout):
		time.sleep(0.1) # sleep to avoid saving layout before it gets restored when displays change
		if (not os.path.exists(file_name + ".lock")):
			logger.info("Saving layout to file \"{}\"".format(file_name))
			file = open(file_name, "w")
			file.write(json.dumps(current_layout, indent=4))

	elif (args.restore_layout):
		if (not os.path.exists(file_name)):
			logger.error("Error: requested file does not exist: {}".format(file_name))
			exit(1)

		run_command("touch {}.lock".format(file_name)) #lock file to avoid overwriting
		logger.info("Restoring layout from file \"{}\"".format(file_name))

		file = open(file_name, "r")
		saved_layout = json.load(file)

		check_fullscreen_windows(spaces, saved_layout["windows"])
		
		windows = get_windows()
		for window in windows:

			saved_window = None
			for w in saved_layout["windows"]:
				if (w["id"] == window["id"]):
					saved_window = w
					break

			if (saved_window):
				if (check_window(window, saved_window)):
					if (saved_window["space"] <= len(spaces)): # Only move window if space exists
						move_window(window=window, target=saved_window)
					else:
						logger.warning("Cannot move window {} to space {}, not existant".format(window["id"], saved_window["space"]))

		logger.debug("hello")
		run_command("rm {}.lock".format(file_name))







	else:
		logger.warning("Please provide something to do.... --save-layout or --restore-layout")
		exit(1)








if __name__ == "__main__":
	main()